<?php
/**
 * @package m3 Multisite Maintenance Mode
 * @version 0.1.0
 */
/*
Plugin Name: M3 Multisite Maintenance Mode
Plugin URI: https://fidelityaction.it
Description: A simple Maintenance Mode plugin that works on multisite. It can display a maintenance page or redirect single website to another Url.
Author: Mauro Gerbaudo
Version: 0.1.0
Author URI: https://fidelityaction.it
Bitbucket Plugin URI: https://bitbucket.org/manzio/m3-multisite-maintenance-mode
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	 die;
}

// Include log class.
//include_once( plugin_dir_path( __FILE__ ) . 'admin/helper/log-helper-class.php' );

// Include the admin dependencies needed to instantiate the plugin.
foreach ( glob( plugin_dir_path( __FILE__ ) . 'admin/*.php' ) as $file ) {
	include_once $file;
}


register_activation_hook( __FILE__, 'm3_plugin_activation' );
register_uninstall_hook(__FILE__, 'm3_plugin_uninstall()');

function m3_plugin_activation() {
	if(!get_option('m3-settings')){
	
		//$rand_token = random_bytes(5);
		//$rand_token = bin2hex($rand_token);
  		$m3_options = array(
  							'url_redirect' 				=> '',
  							'chk_activate_redirect' 	=> '',
  		);
  	
		add_option( 'm3-settings', $m3_options );
	}

}

function m3_plugin_uninstall() {
		delete_option( 'm3-settings', $m3_options );
}


add_action( 'plugins_loaded', 'm3_plugin_init' );
/**
 * Starts the plugin.
 *
 * @since 1.0.0
 */
function m3_plugin_init() {

	$m3_path = plugin_dir_path(__FILE__);
	define('M3_PATH',$m3_path);
	
	// Setup and initialize the class for saving our options.
	$serializer = new m3_serializer();
	$serializer->init();
	
	// Setup the class used to retrieve our option value.
	$deserializer = new m3_deserializer();

	// Setup the administrative functionality.
	$admin = new m3_submenu( new m3_submenu_page( $deserializer ) );
	$admin->init();
	
	$m3_maint = new m3_maintenance();
	$m3_maint->init();


}
