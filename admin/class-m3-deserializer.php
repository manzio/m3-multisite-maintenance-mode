<?php
/**
 * Retrieves information from the database.
 *
 * @package m3 Multisite Maintenance Mode
 */

/**
 * Retrieves information from the database.
 *
 * This requires the information being retrieved from the database should be
 * specified by an incoming key. If no key is specified or a value is not found
 * then an empty string will be returned.
 *
 * @package m3 Multisite Maintenance Mode
 */
class m3_deserializer {

	/**
	 * Retrieves the value for the option identified by the specified key. If
	 * the option value doesn't exist, then an empty string will be returned.
	 *
	 * @param  string $option_key The key used to identify the option.
	 * @return string             The value of the option or an empty string.
	 */
	public function get_value( $option_key ) {
		$m3_options = get_option( 'm3-settings', '' );
		
		return $m3_options[$option_key];
	}
}
