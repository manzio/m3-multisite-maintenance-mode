<?php
/**
 * Creates the submenu page for the plugin.
 *
 * @package m3 Multisite Maintenance Mode
 */

/**
 * Creates the submenu page for the plugin.
 *
 * Provides the functionality necessary for rendering the page corresponding
 * to the submenu with which this page is associated.
 *
 * @package m3 Multisite Maintenance Mode
 */
class m3_submenu_page {

	private $deserializer;

	public function __construct( $deserializer ) {
		$this->deserializer = $deserializer;
	}

	/**
	 * This function renders the contents of the page associated with the Submenu
	 * that invokes the render method. In the context of this plugin, this is the
	 * Submenu class.
	 */
	public function render_settings() {
		include_once( 'views/m3_settings.php' );
	}
	
}
