<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

			<form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">
				<input type="hidden" name="action" value="m3_save_other_option_page">
				<div id="universal-message-container">
					<h2>Title</h2>
					
					<div class="options">
		 				<!-- SET ACTION TO FIRE A FUNCTION ON WP_POST -->
						<p>
						<input type="text" name="option_xx" value="<?php echo esc_attr( $this->deserializer->get_value( 'option_xx' ) ); ?>">
						</p>
					</div>
				</div><!-- #universal-message-container -->
    	
				<?php
					wp_nonce_field( 'm3-settings-save-nonce', 'm3-settings-save-nonce' );
					submit_button();
				?>

			</form>

</div><!-- .wrap -->
