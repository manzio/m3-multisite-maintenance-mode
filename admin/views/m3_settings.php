
<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

	<form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">
		<input type="hidden" name="action" value="m3_save_settings">

		<div id="universal-message-container">
		
			<div class="options">
				<p>
					<label>Activate Redirect?</label>
					<br />
					<input type="checkbox" name="chk_activate_redirect"
					value="1" 
					<?php if($this->deserializer->get_value( 'chk_activate_redirect') == 1){ echo ' checked';} ?>
					/> Redirect to an url instead of printing a maintenance page
				</p>
				<p>
						<label>Redirect to this URL</label>
						<br />
					<?php 
						$url_redirect = $this->deserializer->get_value( 'url_redirect');
						if(!($url_redirect == NULL) || $url_redirect != ''){
							echo esc_attr( $url_redirect); 
						} else {
						 	// No url inserted
						 	echo "no url yet";
						}
					?>
					<br />
					<input type="url" name="url_redirect" value="<?php echo $url_redirect; ?>">
				</p>				

			</div><!--  class="options" -->
			
		</div><!-- #universal-message-container -->

		<?php
			wp_nonce_field( 'm3-settings-save-nonce', 'm3-settings-save-nonce' );
			submit_button();
		?>

	</form>

</div><!-- .wrap -->