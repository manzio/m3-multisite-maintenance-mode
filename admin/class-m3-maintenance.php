<?php
/**
 * Print the maintenance page or redirect to the selected URL
 * 
 *
 * @package m3 Multisite Maintenance Mode
 */


class m3_maintenance {

	public function init(){
	
		add_action('init', array($this, 'm3_do_maintenace_mode'));
	
	}

	public function m3_do_maintenace_mode() {
		
		$deserializer = new m3_deserializer;
		global $pagenow;


		if ( is_user_logged_in() ) {
    		return;
		} else {
	   		if ( $pagenow == 'wp-login.php' ){
				return;
			}
		
			$do_redirect = $deserializer->get_value('chk_activate_redirect');
			//echo $do_redirect;
		
			if ( $do_redirect == '1'){
				
		 	$url = sanitize_text_field(
			
			$deserializer->get_value('url_redirect')	
		);
		
		wp_redirect( urldecode( $url ) , 302);
		exit;
				
				//wp_redirect('http://localhost', 302);
				//exit;
			} else {
				// NOT REDIRECT
				//echo "not redirect ";
				//echo M3_PATH . 'includes/maintenance.html';
   	 			require_once( M3_PATH . 'includes/maintenance.html');
   				exit;
   			}
   		}

	} // m3_display_maintenace_mode
	
	
} // class m3_maintenance




?>