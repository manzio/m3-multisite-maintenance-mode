<?php
/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * @package m3 Multisite Maintenance Mode
 */

/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * This will also check the specified nonce and verfy that the current user has
 * permission to save the data.
 *
 * @package m3 Multisite Maintenance Mode
 */
class m3_serializer {

	/**
	 * Initializes the funtion by registering the save function with the
	 * admin_post hook so that we can save our options to the database.
	 */
	public function init() {
		add_action( 'admin_post_m3_save_settings', array( $this, 'save' ) );

	}


	/**
	 * Validates the incoming nonce value, verifies the current user has
	 * permission to save the value from the options page and saves the
	 * option to the database.
	 */
	public function update_option($option, $m3_options) {

		// First, validate the nonce and verify the user as permission to save.
		if ( ! ( $this->has_valid_nonce() && current_user_can( 'manage_options' ) ) ) {
			// TODO: Display an error message.
			echo "Nonce not valid";
		}
			
		$m3_options = get_option( 'm3-settings', '' );
		$m3_options[$option] = $m3_options;
		
		update_option( 'm3-settings', $m3_options );

	}

	
	/**
	 * Validates the incoming nonce value, verifies the current user has
	 * permission to save the value from the options page and saves the
	 * option to the database.
	 */
	public function save() {

		$m3_options = get_option( 'm3-settings', '' );
		// First, validate the nonce and verify the user as permission to save.
		if ( ! ( $this->has_valid_nonce() && current_user_can( 'manage_options' ) ) ) {
			// TODO: Display an error message.
			echo "Nonce not valid";
		}

		// If the above are valid, sanitize and save the option.
		if (!isset($_POST['url_redirect'])){
			$m3_options['url_redirect'] = '';
		} else {
		 	if ( null !== wp_unslash( $_POST['url_redirect'] ) ) {
			$m3_options['url_redirect'] = sanitize_text_field( $_POST['url_redirect'] );
			}	
		}
				// If the above are valid, sanitize and save the option.
		if (!isset($_POST['chk_activate_redirect'])){
			$m3_options['chk_activate_redirect'] = 0;
		} else {
			if ( null !== wp_unslash( $_POST['chk_activate_redirect'] ) ) {
				$m3_options['chk_activate_redirect'] = sanitize_text_field( $_POST['chk_activate_redirect'] );
			}	
		}
		
		update_option( 'm3-settings', $m3_options );
		

		$this->redirect();

	}

	/**
	 * Determines if the nonce variable associated with the options page is set
	 * and is valid.
	 *
	 * @access private
	 *
	 * @return boolean False if the field isn't set or the nonce value is invalid;
	 *                 otherwise, true.
	 */
	private function has_valid_nonce() {

		// If the field isn't even in the $_POST, then it's invalid.
		if ( ! isset( $_POST['m3-settings-save-nonce'] ) ) { // Input var okay.
			return false;
		}

		$field  = wp_unslash( $_POST['m3-settings-save-nonce'] );
		$action = 'm3-settings-save-nonce';

		return wp_verify_nonce( $field, $action );

	}

	/**
	 * Redirect to the page from which we came (which should always be the
	 * admin page. If the referred isn't set, then we redirect the user to
	 * the login page.
	 *
	 * @access private
	 */
	private function redirect() {

		// To make the Coding Standards happy, we have to initialize this.
		if ( ! isset( $_POST['_wp_http_referer'] ) ) { // Input var okay.
			$_POST['_wp_http_referer'] = wp_login_url();
		}

		// Sanitize the value of the $_POST collection for the Coding Standards.
		$url = sanitize_text_field(
				wp_unslash( $_POST['_wp_http_referer'] ) // Input var okay.
		);

		// Finally, redirect back to the admin page.
		wp_safe_redirect( urldecode( $url ) );
		exit;

	}
}
